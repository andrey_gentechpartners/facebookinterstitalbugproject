package com.naij.facebookaudiencebug;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.naij.facebookaudiencebug.presenters.InterstitialPresenter;

import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    //    private InterstitialPresenter mInterstitialPresenter;
    private static final String TAG = MainActivity.class.getSimpleName();
    private InterstitialAd mAdMobInterstitialAd;
    private com.facebook.ads.InterstitialAd mFacebookInterstitialAd;
    private AdRequest mAdRequest;
    private ScheduledExecutorService worker;
    private ScheduledFuture mScheduledFuture;
    private static final String FACEBOOK_AD_UNIT_INTERSTITIAL_PLACEMENT_ID = "377376682297498_758065464228616";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        mInterstitialPresenter = InterstitialPresenter.getInstance();
//        mInterstitialPresenter.onCreate(this);
        mAdRequest = new AdRequest.Builder().build();
        onCreate(this);
    }

    public void onCreate(Context context) {
        final Context mContext = context.getApplicationContext();
        worker = Executors.newSingleThreadScheduledExecutor();
        //TODO remove
        boolean isShowInterstitialByTimer = true;
        if (isShowInterstitialByTimer) {
            mScheduledFuture = worker.schedule(new Runnable() {
                @Override
                public void run() {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "schedule");
                    }

                    if (!worker.isShutdown() && mContext != null) {
                        showAds(mContext);
                    }
                }
            }, 10, TimeUnit.SECONDS);
        }

        createFacebookInterstitial(mContext);
    }


    private void createFacebookInterstitial(final Context context) {
        mFacebookInterstitialAd = new com.facebook.ads.InterstitialAd(context, FACEBOOK_AD_UNIT_INTERSTITIAL_PLACEMENT_ID);
//        AdSettings.addTestDevice("c34e19b21c4ba869db1af435a04a3166");
        mFacebookInterstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                // Load AdMob
                createAdMobInterstitial(context);

                // Send events
                HashMap<String, String> params = new HashMap<>();
                params.put("errorCode", String.valueOf(adError.getErrorCode()));
                params.put("errorMessage", adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                mFacebookInterstitialAd.loadAd();
            }

            @Override
            public void onInterstitialDisplayed(Ad ad) {
            }
        });

        mFacebookInterstitialAd.loadAd();
    }

    private void createAdMobInterstitial(final Context context) {
        if (context == null) {
            return;
        }

        mAdMobInterstitialAd = new InterstitialAd(context);
//        mAdMobInterstitialAd.setAdUnitId(Constants.AD_MOB_INTERSTITIAL);
        mAdMobInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                mAdMobInterstitialAd.loadAd(mAdRequest);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                HashMap<String, String> params = new HashMap<>();
                params.put("errorCode", String.valueOf(errorCode));

            }
        });
        mAdMobInterstitialAd.loadAd(mAdRequest);
    }

    public void onSaveInstanceState(Context context, Bundle outState) {
    }

    public void onRestoreInstanceState(Context context, Bundle savedInstanceState) {
    }


    private void showAds(Context context) {
        //TODO REMOVE IN RELEASE
        if (mAdMobInterstitialAd != null) {
            if (mAdMobInterstitialAd.isLoaded()) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "AdMob should be shown");
                }
                mAdMobInterstitialAd.show();
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "ADS is not ready");
                }
            }
        } else if (mFacebookInterstitialAd != null) {
            if (mFacebookInterstitialAd.isAdLoaded()) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Facebook should be shown");
                }
                mFacebookInterstitialAd.show();
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "ADS is not ready");
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        worker.shutdown();
        if (mScheduledFuture != null && !mScheduledFuture.isDone()) {
            mScheduledFuture.cancel(true);
        }
        if (mFacebookInterstitialAd != null) {
            mFacebookInterstitialAd.destroy();
        }
        mFacebookInterstitialAd = null;
        mAdMobInterstitialAd = null;
        super.onDestroy();
    }
}
